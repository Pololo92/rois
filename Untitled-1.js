// nav

let mainNav = document.getElementById("js-menu");
let navBarToggle = document.getElementById("js-navbar-toggle");

navBarToggle.addEventListener("click", function() {
  mainNav.classList.toggle("active");
});

// auto type

var typed = new Typed(".typed", {
  strings: [
    "Hugues Capet.",
    "Louis IX.",
    "Philippe Auguste.",
    "Philippe Le Bel."
  ],
  typeSpeed: 70,
  loop: true,
  startDelay: 1000,
  showCursor: false
}); //

// carousel auto
$(document).ready(function() {
  nbImages = $(".slide").length;

  function reorder() {
    for (let k = 0; k < nbImages; k++) {
      $(".slide")
        .eq(k)
        .css("order", k);
    }
  }
  reorder();
  compteur = 0;

  function carousel() {
    $(".reglette").animate({ left: "-100vh" }, 1000, function() {
      $(".reglette").css("left", 0);
      $(".slide")
        .eq(compteur)
        .css(
          "order",
          Number(
            $(".slide")
              .eq(compteur)
              .css("order")
          ) + nbImages
        );
      compteur++;
    });

    if (compteur == nbImages) {
      compteur = 0;
      reorder();
    }
  }

  timer = setInterval(carousel, 6000);
});

// horloge

var clock = document.getElementById("clock");
var hexColor = document.getElementById("hex-color");

function hexClock() {
  var time = new Date();
  var hours = time.getHours().toString();
  var minutes = time.getMinutes().toString();
  var seconds = time.getSeconds().toString();

  var hoursRandom = Math.ceil(Math.random() * 1);
  console.log(hoursRandom);
  var minutesRandom = Math.ceil(Math.random() * 30);
  var secondsRandom = Math.ceil(Math.random() * 9);

  if (hours.length < 2) {
    hours = "0" + hours; // en cas de mono digit
  }

  if (minutes.length < 2) {
    minutes = "0" + minutes;
  }

  if (seconds.length < 2) {
    seconds = "0" + seconds;
  }

  var clockStr = hours + " : " + minutes + " : " + seconds; // affichage de l'horloge
  var hexColorStr = "#" + hoursRandom + minutesRandom + secondsRandom; // affichage du code hex par rapport a l'heure

  clock.textContent = clockStr; // applique la variable d'affichage
 //applique la variable d'affichage

  if (time.getSeconds() % 2) {
    hexColor.textContent = hexColorStr;
    document.getElementById("clock").style.backgroundColor = hexColorStr; // changement du bg de l'horloge avec le code hex chaque seconde
  }
}

hexClock();
setInterval(hexClock, 1000); // 1sec


